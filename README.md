# Matcha Theme

A dark theme for Visual Studio Code inspired by the [Matcha GTK theme](https://github.com/vinceliuice/matcha) by [vinceliuice](https://github.com/vinceliuice).
